var app = angular.module('Hotel', [
    'ngRoute',
    'ui.bootstrap',
    'angularSlideables',
    'ngMaterial'
]).config(function($mdThemingProvider) {
    // Configure a dark theme with primary foreground yellow
    $mdThemingProvider.theme('docs-dark', 'default')
        .primaryPalette('yellow')
        .dark();
});