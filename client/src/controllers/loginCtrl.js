app.controller('LoginController', ['$location', 'LoginService', '$mdToast', function($location, LoginService, $mdToast){

    var self = this;
    this.invalid = false;
    LoginService.isLoginPage = true;

    this.user = {
        login: '',
        password: ''
    };

    this.login = function () {
        LoginService.login(self.user).then(function (data) {
            if(data.status == 401){
                LoginService.invalid = true;
                self.showToast();
            } else {
                LoginService.invalid = false;
                $location.path('/admin/dashboard');
            }
        }, function (error) {
            console.log(error);
        })
    };

    this.showToast = function() {
        $mdToast.show({
            hideDelay   : 4000,
            position    : 'left',
            template : '<md-toast><span class="md-toast-text" flex>Incorrect login or password</span></md-toast>'
        });
    };
}]);
