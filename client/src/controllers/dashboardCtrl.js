app.controller('DashboardController', ['$location', 'LoginService', '$mdToast', 'ApartmentsService', 'ReservationService',
    function($location, LoginService, $mdToast, ApartmentsService, ReservationService){

    var self = this;

    var init = function () {
        if (LoginService.invalid) {
            $location.path('/admin');
        } else {
            ApartmentsService.getAll().then(function () {
                self.apartments = ApartmentsService.apartments;
            });
            ReservationService.getAll().then(function () {
               self.reservations = ReservationService.reservations;
            })
        }
    };

        this.getApartmentName = function (id) {
            if (id == 1) {
                return "Bloody Red"
            } else if (id == 2) {
                return "Purple Rain"
            } else if (id == 3) {
                return "Atlantic Blue"
            }
        };

    init()
}]);
