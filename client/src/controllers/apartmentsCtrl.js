app.controller('ApartmentsCtrl', function($scope, $routeParams, $filter, ApartmentsService) {
	
	var self = this;
	this.apartments = ApartmentsService.apartments;
    this.reverse = false;
    this.order = "";
    
    $scope.dateStart = $routeParams.dateStart;
    $scope.dateEnd = $routeParams.dateEnd;
    $scope.guests = $routeParams.guests;
    // var dateStart = $filter('date')(new Date($routeParams.dateStart), 'y-MM-d');
    // var dateEnd = $filter('date')(new Date($routeParams.dateEnd), 'y-MM-d');
    // var guests = $routeParams.guests;
	ApartmentsService
		.getApartments($routeParams.dateStart, $routeParams.dateEnd, $routeParams.guests)
		.then(function(){
            self.apartments = ApartmentsService.apartments;
	})
    
    this.sortOrder = function() {
        this.order = 'price'; 
        this.reverse = !this.reverse;
    }
})
