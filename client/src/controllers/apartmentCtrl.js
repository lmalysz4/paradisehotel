app.controller('ApartmentCtrl', function($scope, $routeParams, ApartmentsService) {
	
	var self = this;
	this.apartment = ApartmentsService.apartment;
    $scope.customer = {};
    
    $scope.dateStart = $routeParams.dateStart;
    $scope.dateEnd = $routeParams.dateEnd;
    $scope.guests = $routeParams.guests;
    
	 ApartmentsService
	 	.getOne($routeParams.id)
	 	.then(function(){
	 	self.apartment = ApartmentsService.apartment;
	 })
	 
    $scope.book = function() {
        ApartmentsService.bookApartment(self.apartment.id, $scope.dateStart, $scope.dateEnd, $scope.customer).then(function(data){
            self.reserved = true;
        })
    };
    
    $scope.close = function() {
        self.reserved = false;
    }
})