app.controller('ReservationCtrl', function($scope, $routeParams, ReservationService, ApartmentsService) {
	
	var self = this;
    
    this.reservation = {};
    this.reservationCredentials = {};
    this.apartment = {};
    
    this.getReservation = function() {
        ReservationService.getReservation(self.reservationCredentials).then(function(data){
            self.reservation = data;
            ApartmentsService.getOne(data.apartment_id).then(function(apartment){
                self.apartment = apartment;
            })
        }, function(err){
            
        })
    };
    
    this.cancelReservation = function() {
        ReservationService.cancelReservation(self.reservation.id).then(function(data){
            self.apartment = null;
        })
    }
	
})