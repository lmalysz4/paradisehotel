app.controller('SearchCtrl', function ($scope, $location, $routeParams, $filter, ApartmentsService) {
  $scope.date = {};
  $scope.date.start = new Date();
  $scope.setTomorrow = function(){
    $scope.date.end = new Date($scope.date.start.getTime() + 24 * 60 * 60 * 1000);
    //$scope.$digest();
  };
  
  $scope.init = function() {
    if($routeParams.dateStart && $routeParams.dateEnd && $routeParams.guests){
        $scope.date.start = new Date($routeParams.dateStart);
        $scope.date.end = $routeParams.dateEnd;
        $scope.guests = $routeParams.guests;
    } else {
        $scope.date.start = new Date();
        $scope.setTomorrow();
        $scope.guests = 2; 
    }
    
    $scope.homeView = ($location.path() == '/') ? true : false;
    $scope.format = ($location.path() == '/') ? $scope.formats[0] : $scope.formats[1];
    $scope.minEndDate = new Date($scope.date.start.getTime() + 24 * 60 * 60 * 1000);
  };
  

  $scope.guestsNumbers = [1, 2, 3, 4];
  
  $scope.setNumberOfGuests = function(number){
      $scope.guests = number;
  };
  
  $scope.clear = function () {
    $scope.date.start = null;
  };

  // Disable weekend selection
  $scope.disabled = function(date, mode) {
    //return ( mode === 'day' && ( date.getDay() === 0 || date.getDay() === 6) );
  };
  
  $scope.disabledEnd = function(date, mode) {
    return ( mode === 'day' && (date <= $scope.date.start) );
  };

  $scope.getMonth = function(date){
    var months = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"]  
    return months[date.getMonth()];
  };

  $scope.toggleMin = function() {
    $scope.minDate = $scope.minDate ? null : new Date();
  };
  $scope.toggleMin();
  
  
  $scope.maxDate = new Date(2020, 5, 22);

  $scope.openStart = function($event) {
    $scope.status.startOpened = true;
  };
  
  $scope.openEnd = function($event) {
    $scope.status.endOpened = true;
    $scope.$digest();

  };

  $scope.setDate = function(year, month, day) {
    $scope.date.start = new Date(year, month, day);
    if($scope.date.start >= $scope.date.end){
      $scope.setTomorrow();
    }
  };
  
  $scope.$watch("date.start", function(newVal, oldVal){
    $scope.date.end = new Date($scope.date.end);  
    if($scope.date.start >= $scope.date.end){
      $scope.setTomorrow();
    }
  })

  $scope.dateOptions = {
    formatYear: 'yy',
    startingDay: 1
  };

  $scope.formats = ['dd', 'dd MMMM yyyy'];
  

  $scope.status = {
    startOpened: false,
    endOpened: false
  };

  var tomorrow = new Date();
  tomorrow.setDate(tomorrow.getDate() + 1);
  var afterTomorrow = new Date();
  afterTomorrow.setDate(tomorrow.getDate() + 2);
  $scope.events =
    [
      {
        date: tomorrow,
        status: 'full'
      },
      {
        date: afterTomorrow,
        status: 'partially'
      }
    ];

  $scope.getDayClass = function(date, mode) {
    if (mode === 'day') {
      var dayToCheck = new Date(date).setHours(0,0,0,0);

      for (var i=0;i<$scope.events.length;i++){
        var currentDay = new Date($scope.events[i].date).setHours(0,0,0,0);

        if (dayToCheck === currentDay) {
          return $scope.events[i].status;
        }
      }
    }

    return '';
  };
  
  $scope.search = function(){
     ApartmentsService.apartments = [];
     $scope.homeView = false;
  };
  
  $scope.searchFromResults = function(){
     ApartmentsService.apartments = [];
     ApartmentsService.getApartments(new Date($scope.date.start), new Date($scope.date.end), $scope.guests).then(function(){
         $scope.$parent.$parent.$digest();
     })
  };
  
  $scope.init();
  
});