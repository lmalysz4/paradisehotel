function GenericService($http){

        this.collection = [];

        this.httpRequest = function (method, url, errorMsg, data) {
                var config = {
                    method: method,
                    url: url,
                    data: data
                };

                return $http(config).then(function (response) {
                    return response.data;
                }, function (response) {
                    return response;
                });
            };
}

