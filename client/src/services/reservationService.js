
app.service('ReservationService', ['$injector', '$q', '$filter', function($injector, $q, $filter){

    var self = this;
    $injector.invoke(GenericService, this);
    
    
     this.getReservation = function(reservation){
        return self.httpRequest('post', '/api/reservation/', 'ApartmentsService.getOne() method failed', reservation).then(function(data){
             return self.apartment = data;
         }, function(error){
            var x = error;
         });
     };
     
     this.cancelReservation = function(id){
         return self.httpRequest('delete', '/api/reservation/' + id).then(function(reservation){
             return reservation;
         })
     };

    this.getAll = function () {
        return self.httpRequest('get', '/api/reservations/').then(function(reservations){
            self.reservations = reservations;
        })
    }
}]);
