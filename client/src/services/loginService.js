
app.service('LoginService', ['$injector', '$q', '$filter', function($injector, $q, $filter){

    var self = this;
    $injector.invoke(GenericService, this);

    this.invalid = true;
    this.isLoginPage = false;

    this.login = function(user){
        return self.httpRequest('post', '/api/login', 'LoginService.login() method failed', user).then(function(data){
            return data;
        });
    };
}]);
