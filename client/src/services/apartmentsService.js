
app.service('ApartmentsService', ['$injector', '$q', '$filter', function($injector, $q, $filter){

    var self = this;
    $injector.invoke(GenericService, this);
    
    this.apartment;
    this.apartments = [];

     this.getAll = function(){
         return self.httpRequest('get', '/api/apartments', 'ApartmentsService.getAll() method failed').then(function(data){
             self.apartments = data;
         }, function(error){
           
         });
     };
     
     this.getOne = function(id){
        return self.httpRequest('get', '/api/apartments/' + id, 'ApartmentsService.getOne() method failed').then(function(data){
             return self.apartment = data;
         }, function(error){
            var x = error;
         });
     };
     
     this.getApartments = function(start, end, guests){
         var dateStart = $filter('date')(new Date(start), 'y-MM-d');
         var dateEnd = $filter('date')(new Date(end), 'y-MM-d');
         return self.httpRequest('get', '/api/apartments/' + dateStart + '/' + dateEnd + '/' + guests, 'ApartmentsService.getAll() method failed').then(function(data){
             self.apartments = data;
         }, function(error){
            
         });
     };

     this.bookApartment = function(id, start, end, customer){
         var dateStart = $filter('date')(new Date(start), 'y-MM-d');
         var dateEnd = $filter('date')(new Date(end), 'y-MM-d');
         customer.bookDate = $filter('date')(new Date(), 'y-MM-d H:mm:ss');
         return self.httpRequest('post', '/api/bookings/' + id + '/' + dateStart + '/' + dateEnd, 'ApartmentsService.getAll() method failed', customer).then(function(data){
             return data;
         }, function(error){
            var x = error;
         });
     }
}]);
