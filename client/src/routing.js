app.config(function ($routeProvider, $locationProvider) {
    $routeProvider
        .when('/reservation', {
            templateUrl: 'src/templates/reservation/reservation.html'
        })
        .when('/apartments/:id/:dateStart/:dateEnd/:guests', {
            templateUrl: '/src/templates/apartments/apartment.html'
        }).when('/', {
            templateUrl: '/src/templates/search.html'
        }).when('/search/:dateStart/:dateEnd/:guests', {
            templateUrl: '/src/templates/apartments/apartments.html'
        }).when('/admin', {
            templateUrl: '/src/templates/login/login.html'
        }).when('/admin/dashboard', {
            templateUrl: '/src/templates/admin/dashboard.html'
        }).otherwise({
            templateUrl: '/src/templates/search.html'
        })
 });