-- phpMyAdmin SQL Dump
-- version 3.5.0
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Jan 22, 2016 at 01:33 AM
-- Server version: 5.1.62-community
-- PHP Version: 5.3.27

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `hoteldb`
--

-- --------------------------------------------------------

--
-- Table structure for table `apartment`
--

CREATE TABLE IF NOT EXISTS `apartment` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  `description` text NOT NULL,
  `price` int(11) NOT NULL,
  `places` int(11) NOT NULL,
  `img` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `apartment`
--

INSERT INTO `apartment` (`id`, `name`, `description`, `price`, `places`, `img`) VALUES
(1, 'Bloody Red', 'Superior category rooms measuring 33 m2 with the services and facilities of the Premium rooms. Spacious, bright, outward facing rooms and totally refurbished, these rooms come with double bed or twin beds with Dreamax mattress (manufactured and designed exclusively by Flex for Meliá Hotels International), a modern, fully equipped bathroom.', 239, 2, 'src/images/BloodyRed/room1.jpg'),
(2, 'Atlantic Blue', 'Spacious, bright and outward facing rooms measuring 27 m2 and with views of Plaza España and Parque de Maria Luisa. Totally refurbished, the room comes with double bed or twin beds with Dreamax mattress (manufactured and designed exclusively by Flex for Meliá Hotels International), a modern, fully equipped bathroom.', 199, 1, 'src/images/AtlanticBlue/room2.jpg'),
(3, 'Purple Rain', 'Spacious, bright and outward facing rooms measuring 27 m2 and with views of Plaza España and Parque de Maria Luisa. Totally refurbished, the room comes with double bed or twin beds with Dreamax mattress (manufactured and designed exclusively by Flex for Meliá Hotels International), a modern, fully equipped bathroom finished in top quality bronze coloured ceramics and an independent entrance.', 399, 2, 'src/images/PurpleRain/room3.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `booking`
--

CREATE TABLE IF NOT EXISTS `booking` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `code` text NOT NULL,
  `customer_id` varchar(50) NOT NULL,
  `apartment_id` int(11) NOT NULL,
  `employee_id` int(11) NOT NULL,
  `payment_id` int(11) NOT NULL,
  `date` date NOT NULL,
  `booked_from` date NOT NULL,
  `booked_to` date NOT NULL,
  PRIMARY KEY (`id`),
  KEY `apartment_id` (`apartment_id`),
  KEY `customer_id` (`customer_id`),
  KEY `employee_id` (`employee_id`),
  KEY `payment_id` (`payment_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=40 ;

--
-- Dumping data for table `booking`
--

INSERT INTO `booking` (`id`, `code`, `customer_id`, `apartment_id`, `employee_id`, `payment_id`, `date`, `booked_from`, `booked_to`) VALUES
(38, 'ZK4OY', 'bartlomiejwod@gmail.com', 1, 0, 0, '0000-00-00', '2016-01-21', '2016-01-22'),
(37, '7MUYD', 'bartlomiejwod@gmail.com', 1, 0, 0, '0000-00-00', '2016-01-21', '2016-01-22'),
(36, 'XE9G1', 'lukasz.malysz@gmail.com', 1, 0, 0, '0000-00-00', '2016-07-23', '2016-07-25'),
(35, 'JXE97', 'lukasz.malysz@gmail.com', 1, 0, 0, '0000-00-00', '2016-01-20', '2016-01-21'),
(34, 'KU37I', 'lukasz.malysz@gmail.com', 3, 0, 0, '0000-00-00', '2016-01-20', '2016-01-21'),
(24, 'ZYMWX', '0', 1, 0, 0, '0000-00-00', '2016-09-06', '2016-09-08'),
(25, '9GISX', '0', 1, 0, 0, '0000-00-00', '2016-09-06', '2016-09-08'),
(26, 'QGBE3', '0', 1, 0, 0, '0000-00-00', '2016-09-06', '2016-09-08'),
(27, 'ELTYU', '0', 3, 0, 0, '0000-00-00', '2016-08-05', '2016-08-07'),
(28, '03EOF', 'test@o2.pl', 1, 0, 0, '0000-00-00', '2016-08-06', '2016-08-07'),
(29, '9RTAO', 'test@o2.pl', 1, 0, 0, '0000-00-00', '2016-07-03', '2016-07-05'),
(30, '34D35', 'lukasz.malysz@gmail.com', 3, 0, 0, '0000-00-00', '2016-02-05', '2016-02-06'),
(31, 'VK4RZ', 'lukasz.maly', 3, 0, 0, '0000-00-00', '2016-02-05', '2016-02-06'),
(32, 'WIGCJ', 'lukasz.maly', 3, 0, 0, '0000-00-00', '2016-03-17', '2016-03-18'),
(33, 'CTZ3V', 'lukasz.malysz@gmail.com', 3, 0, 0, '0000-00-00', '2016-07-02', '2016-07-04'),
(39, '7EH3G', '156291@student.pwr.wroc.pl', 3, 0, 0, '0000-00-00', '2016-01-21', '2016-01-22');

-- --------------------------------------------------------

--
-- Table structure for table `check_in`
--

CREATE TABLE IF NOT EXISTS `check_in` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `apartment_id` int(11) NOT NULL,
  `customer_id` int(11) NOT NULL,
  `check_in` date NOT NULL,
  `check_out` date NOT NULL,
  PRIMARY KEY (`id`),
  KEY `apartment_id` (`apartment_id`),
  KEY `customer_id` (`customer_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE IF NOT EXISTS `user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `login` varchar(50) NOT NULL,
  `password` varchar(50) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `customer`
--

CREATE TABLE IF NOT EXISTS `customer` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  `surname` varchar(50) NOT NULL,
  `mail` varchar(50) NOT NULL,
  `phone` int(20) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `employee`
--

CREATE TABLE IF NOT EXISTS `employee` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  `surname` varchar(50) NOT NULL,
  `mail` varchar(50) NOT NULL,
  `phone` int(20) NOT NULL,
  `employed_from` date NOT NULL,
  `employed_to` date NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `images`
--

CREATE TABLE IF NOT EXISTS `images` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `apartment_id` int(11) NOT NULL,
  `path` text NOT NULL,
  PRIMARY KEY (`id`),
  KEY `apartment_id` (`apartment_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=10 ;

--
-- Dumping data for table `images`
--

INSERT INTO `images` (`id`, `apartment_id`, `path`) VALUES
(1, 1, 'src/images/BloodyRed/room1_2.jpg'),
(2, 1, 'src/images/BloodyRed/room1_3.jpg'),
(3, 1, 'src/images/BloodyRed/room1_4.jpg'),
(4, 2, 'src/images/AtlanticBlue/room2_2.jpg'),
(5, 2, 'src/images/AtlanticBlue/room2_3.jpg'),
(6, 2, 'src/images/AtlanticBlue/room2_4.jpg'),
(7, 3, 'src/images/PurpleRain/room3_1.jpg'),
(8, 3, 'src/images/PurpleRain/room3_2.jpg'),
(9, 3, 'src/images/PurpleRain/room3_3.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `payment`
--

CREATE TABLE IF NOT EXISTS `payment` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `customer_id` int(11) NOT NULL,
  `employee_id` int(11) NOT NULL,
  `booking_id` int(11) NOT NULL,
  `amount` int(11) NOT NULL,
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `customer_id` (`customer_id`),
  KEY `employee_id` (`employee_id`),
  KEY `booking_id` (`booking_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `position`
--

CREATE TABLE IF NOT EXISTS `position` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(50) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
