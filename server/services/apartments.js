var Apartment = require('./../models/Apartment');
var Booking = require('./../models/Booking');

module.exports = {
    getAll: function (req, res) {
      Apartment.forge().fetchAll().then(function (apartments) {
            res.json(apartments);
        }, function (error) {
            console.error(error);
        });
    },

     getOne: function (req, res) {   
       Apartment.forge({id: req.params.id}).fetch({withRelated: ['images']}).then(function (apartment) {
            res.json(apartment);
       }, function (error) {
            console.log(error);
      })
  },
  
  getAvailableApartments: function (req, res) {
      var isNotAvailable = function(from, to){
        
          var dateStart = new Date(req.params.dateStart);
          var dateEnd = new Date(req.params.dateEnd);
          //console.log((dateStart >= from && dateStart <= to) || (dateEnd >= from && dateEnd <= to));
          return ((dateStart >= from && dateStart <= to) || (dateEnd >= from && dateEnd <= to)) ? true : false;
      }
      Apartment.forge().where({places: req.params.guests}).fetchAll().then(function (apartments) {
         
            //res.json(apartments);
            Booking.forge().fetchAll().then(function (bookings) {
                var availableApartments = [];
                apartments.forEach(function(apartment){
                    //console.log('1');
                    var booked = false;
                    bookings.models.forEach(function(booking){
                        if(apartment.id == booking.attributes.apartment_id && isNotAvailable(booking.attributes.booked_from, booking.attributes.booked_to)){
                            booked = true;
                            console.log("forEachIN");
                        }
                    });
                    if(!booked){
                        availableApartments.push(apartment);  
                    }
                })                
                //console.log(availableApartments);
                res.json(availableApartments);
            })
        }, function (error) {
            console.error(error);
        });
    }
};