var Booking = require('./../models/Booking');
var Usluga = require('./../models/Usluga');
var Apartment = require('./../models/Apartment');
var $mailService = require("nodemailer");
var mail = require('./mail');

var smtpTransport = $mailService.createTransport("SMTP",{
    service: "Gmail",
    auth: {
        XOAuth2: {
            user: "lukasz.malysz@gmail.com", // Your gmail address.
                                                  // Not @developer.gserviceaccount.com
            clientId: "119636669367-2iogn27a17club66i1joi3taer37o1ah.apps.googleusercontent.com",
            clientSecret: "YTLt2CNYCMlzqLI8SDBNVETR",
            refreshToken: "1/PQj8jO7R3i2oW4Bq3uIhQYFq6FXkOPTvx-hBLC_OXZY"
        }
    }
       /* service: 'Gmail',
        auth: {
            user: 'paradisehotelapp@gmail.com',
            pass: 'paradisehotel'
        }*/

});

var sendMail = function(to, subject, text) {
    var mailOptions = {
        to: to,
        subject: subject,
        html: text
    };
    console.log(mailOptions);
    smtpTransport.sendMail(mailOptions, function (error, response) {
        if (error) {
            console.log(error);
        } else {
            console.log("Message sent: " + response.message);
        }
    })    
};

function createReservationCode() {
    var text = "";
    var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";

    for( var i=0; i < 5; i++ )
        text += possible.charAt(Math.floor(Math.random() * possible.length));

    return text;
}

module.exports = {  
  bookApartment: function(req, res){
      var code = createReservationCode();
      console.log("code1: " + code);
      Booking.forge().save({code: code,
                            customer_id: req.body.mail,
                            apartment_id: req.params.id,
                            booked_from: req.params.dateStart,
                            booked_to: req.params.dateEnd
                            }).then(function(booking){
                                sendMail(req.body.mail, "Room Reservation", mail.getBody(req.body.name, code));
                                res.json(booking);
                                /*Apartment.forge({id: req.params.id}).fetch().then(function(apartment){
                                    console.log(req.params.id);
                                    console.log(apartment.attributes);
                                    var time = new Date(req.params.dateEnd) - new Date(req.params.dateStart);
                                    var hours = (time / (1000*60*60));
                                    var description = "" + req.body.name + " " + req.body.surname + " " + req.body.mail;
                                    Booking.forge().save({
                                        nazwa: apartment.attributes.name, 
                                        cena: apartment.attributes.price,
                                        czas: hours + ":00:00",
                                        opis: description,
                                        data_wpisu: req.body.bookDate
                                     }).then(function() {
                                        res.json(booking);
                                     })
                                    
                                })*/
                                
                            })
  },
  
  getReservation: function(req, res){
      Booking.forge().where({customer_id: req.body.mail}).fetchAll().then(function(bookings){
          var found = false;
          bookings.forEach(function(booking){
              console.log(booking);
              if(booking.attributes.code == req.body.code){
                  found = true;
                  res.json(booking);
              }
          });
          if(!found){
              res.sendStatus(404);
          }
          
      }, function(err){
          console.log(err);
      })
  },
  
  cancelReservation: function(req, res) {
      Booking.forge({id: req.params.id}).destroy().then(function(booking){
          res.json(booking);
      })
  },

    getAll: function (req, res) {
        Booking.forge().fetchAll().then(function (bookings) {
            console.log(bookings);
            res.json(bookings);
        })

    }
};