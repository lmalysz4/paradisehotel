var bookshelf = require('../db');


var Apartment = bookshelf.Model.extend({
    tableName: 'apartment',
    images: function() {
        return this.hasMany(require('./image'), 'apartment_id');
    },
    bookings: function () {
        return this.hasMany(require('./booking'), 'apartment_id');
    }
});

module.exports = Apartment;