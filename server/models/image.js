var bookshelf = require('../db');

var Image = bookshelf.Model.extend({
    tableName: 'images'
});

module.exports = Image;