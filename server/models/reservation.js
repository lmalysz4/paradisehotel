var bookshelf = require('../db');


var Reservation = bookshelf.Model.extend({
    tableName: 'booking',
    images: function() {
        return this.hasMany(require('./image'), 'apartment_id');
    }
});

module.exports = Apartment;