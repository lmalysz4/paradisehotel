var bookshelf = require('../db');


var Booking = bookshelf.Model.extend({
    tableName: 'booking',
    apartment: function() {
        return this.hasOne(require('./apartment'));
    }
});

module.exports = Booking;