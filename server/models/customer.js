var bookshelf = require('../db');


var Customer = bookshelf.Model.extend({
    tableName: 'customer',
    bookings: function() {
        return this.hasMany(require('./booking'), 'customer_id');
    },
    payments: function() {
        return this.hasMany(require('./payments'), 'customer_id');
    }
});

module.exports = Customer;