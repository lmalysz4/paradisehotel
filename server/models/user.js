var bookshelf = require('../db');


var User = bookshelf.Model.extend({
    tableName: 'user'
});

module.exports = User;