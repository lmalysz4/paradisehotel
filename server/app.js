var express = require('express');
var app = express();

var db = require('mysql');
var path = require('path');
var bodyParser = require('body-parser');
var Promise = require('bluebird');
var router = express.Router();

//services
var $apartments = require('./services/apartments');
var $bookings = require('./services/bookings');
var $authentication = require('./services/authentication');

var bookshelf = require('./db');
app.set('bookshelf', bookshelf);

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({
    extended: true
}));

app.use('/', express.static('../client'));

app.use('/api/', router);

router.post('/login', $authentication.login);

router.get('/apartments', $apartments.getAll);
router.get('/apartments/:id', $apartments.getOne);
router.get('/apartments/:dateStart/:dateEnd/:guests', $apartments.getAvailableApartments);

router.post('/bookings/:id/:dateStart/:dateEnd', $bookings.bookApartment);
router.post('/reservation', $bookings.getReservation);
router.get('/reservations', $bookings.getAll);

router.delete('/reservation/:id', $bookings.cancelReservation);

router.get('/search/:id', $apartments.getAvailableApartments);

var server = app.listen(3000, function () {
    var host = server.address().address,
        port = server.address().port;
    console.log('Example app listening at http://%s:%s', host, port);
});

module.exports = app;
